alias kxp='kubectx gke_mms-cta-sterlingjms-p-2001_europe-west4_prod'
alias kxd='kubectx gke_mms-cta-sterlingjms-d-2001_europe-west4-b_dev'

alias Gd='gcloud config configurations activate jms'
alias Gp='gcloud config configurations activate stjms-p'
alias Gc='gcloud config configurations activate mms-config'
